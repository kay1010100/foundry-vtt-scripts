const effectName = "spell effect name here" //for example "Spell Effect: Inspire Courage"
const packKey = "pf2e.spell-effects";

const tokens = canvas.scene.tokens.filter(t => t.actor.hasPlayerOwner); // or use [...game.user.targets.map(t => t.document)]; //for targeted tokens.
const pack = game.packs.get(packKey);
const item = pack.index.getName(effectName);
const ITEM_UUID = item.uuid; 
const source = (await fromUuid(ITEM_UUID)).toObject();
source.flags = mergeObject(source.flags ?? {}, { core: { sourceId: ITEM_UUID } });

for (let tokenDoc of tokens){
    const options = {name: effectName, description: `Adds ${effectName} to ${tokenDoc.name}`};
    const existing = !!warpgate.mutationStack(tokenDoc).getName(effectName);
    if(existing) await warpgate.revert(tokenDoc, effectName);
    else await warpgate.mutate(tokenDoc, {embedded: {Item: {[source.name]: source}}}, {}, options);
}

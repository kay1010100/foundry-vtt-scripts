// this macro gets all the d20 rolled from the messages and spits back what is the avg roll and all rolled d20 results.
// // step zero, lets declare some variables..
let rollArray = []
let critCount = 0;
let failCount = 0;

// // step one, get all the messages with a d20 roll in them.
const rollMessages = game.messages.filter(m => m.rolls.some(e => e.formula?.includes("d20"))) // question marks are needed to not have the filter get stuck on messages that have no rolls.

// // step two, get the rolled dice. This is where it gets ugly! Not good coding practice but I dont know how else to get down there.
for (let message of rollMessages) {                     // takes an individual message from the rollMessages object.
    for (let roll of message.rolls) {                   // every dice roll consists of a term array, we need to check each of the elements whether they contain a 20 sided dice.
        for(let term of roll.terms) {
            if (term.faces === 20) {                    // if the die has 20sides we continue our search otherwise we just dont care and go to the next message.
                for (let result of term.results) {      // results can consist of multiple d20 results, most notably in adv. / disadv. rolls.
                    const rollResult = result.result;
                    rollArray.push(rollResult);         // here we combine all the results in an array.
                    if (rollResult === 20) {            // just for funsies we add a counter that goes up each time a crit success was rolled.
                        critCount++;
                    }
                    else if (rollResult === 1) {
                        failCount++;                    // ditto for crit fails.
                    }
                }
            }
        }
    }
}

// // step three, assembling the data for presentation.
const sumRolls = rollArray.reduce((a, b) => a + b, 0);  // adds all the numbers together within the array.
const averageRolls = sumRolls / rollArray.length;       // averages the result by dividing by d20s rolled.
let allRolls = rollArray.join("-");                   // joins all the numbers in the array into a string divided by a dash, eg. "3-15-18-5-20"
allRolls = allRolls.replaceAll(/(?<=\D)20(?=\D)/g, "<b>20</b>");
allRolls = allRolls.replaceAll(/(?<=\D)1(?=\D)/g, "<b>1</b>");

// // step four, spitting the data out for the user.
await ChatMessage.create({
    flavor: 'Dice stats:', 
    content: `<p>In today's session ${rollArray.length} d20's were rolled.</p>
            <p>An avgerage of ${averageRolls.toFixed(2)} was rolled.</p>
            <p>${critCount} ${critCount === 1 ? `critical success was rolled.` : `critical successes were rolled.`}</p> 
            <p>${failCount} ${failCount === 1 ? `critical fail was rolled.` : `critical fails were rolled.`}</p> 
            <p>Raw numbers:${allRolls}</p>`
});
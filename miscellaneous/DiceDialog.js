function handleEvents(html){
  html.change(function(event){
      if(event.target.className === "dice-count") {
          const count = event.target.value;
          const oldFormula = html.find(".dice-formula").val();
          const newFormula = oldFormula.replace(/\d{1,3}(?=d)/gm, count);
          html.find(".dice-formula").val(newFormula);
      }
      if(event.target.className === "dice-faces") {
          const faces = event.target.value;
          const oldFormula = html.find(".dice-formula").val();
          const newFormula = oldFormula.replace(/(?<=d)\d{1,3}/gm, faces);
          html.find(".dice-formula").val(newFormula);
      }
      // look into using signed strings for the constant.
      if(event.target.className === "dice-bonus") {
          const input = Number(event.target.value)
          const sign = input >= 0 ? "+" : "-";
          const value = Math.abs(input);
          const oldFormula = html.find(".dice-formula").val();
          const newFormula = oldFormula.replace(/[+,-]/gm, sign).replace(/\s\d{1,3}/gm, " "+ value);
          html.find(".dice-formula").val(newFormula);
      }
  });
}
const content = `<form>
  <div class="form-group">
      <label>Dice count:</label>
      <div class="form-fields">
          <input id="temp-id-for-test" class="dice-count" type="number" value="1">
      </div>
  </div>
  <div class="form-group">
      <label>Die faces:</label>
      <div class="form-fields">
          <input class="dice-faces" type="number" value="6">
      </div>
  </div>
  <div class="form-group">
      <label>Bonus:</label>
      <div class="form-fields">
          <input class="dice-bonus" type="number" value="0">
      </div>
  </div>
  <div class="form-group">
      <label>Formula:</label>
      <div class="form-fields">
          <input class="dice-formula" value="1d6 + 0" disabled>
      </div>
  </div>
</form>`;

new Dialog({
  title: "Test",
  content,
  buttons: {
      roll: {
          label: "Roll",
          callback: async(html) => {
              const formula = html.find(".dice-formula").val();
              await new Roll(formula).toMessage();
          }
      }
  },
  render: handleEvents
}).render(true);
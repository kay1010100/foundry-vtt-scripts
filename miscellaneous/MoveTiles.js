//v10 macro:
async function moveX(direction, fine) {
    let distance = fine ? 1 : 10;
    distance = direction == "left" ? (distance * (-1)) : distance;
    const tilesToMove = canvas.tiles.controlled ;
    const updates = tilesToMove.map(tile => ({
        _id: tile.id,
        x: tile.document.x + distance
    }));
    await canvas.scene.updateEmbeddedDocuments("Tile", updates);
}

async function moveY(direction, fine) {
    let distance = fine ? 1 : 10;
    distance = direction == "up" ? (distance * (-1)) : distance;
    const tilesToMove = canvas.tiles.controlled;
    const updates = tilesToMove.map(tile => ({
        _id: tile.id,
        y: (tile.document.y + distance)
    }));
    await canvas.scene.updateEmbeddedDocuments("Tile", updates);
}

async function rotate(direction, fine) {
    let updates = [];
    if(direction !== "rotate-reset") {
        let increment = fine ? 5 : 45;
        increment = direction === "rotate-right" ? increment : increment * -1;
        const tilesToMove = canvas.tiles.controlled;
        updates = tilesToMove.map(tile => ({
            _id: tile.id,
            rotation: (tile.document.rotation + increment)%360
        }));
    }
    else {
        const tilesToMove = canvas.tiles.controlled;
        updates = tilesToMove.map(tile => ({
            _id: tile.id,
            rotation: 0
        }));  
    }
    await canvas.scene.updateEmbeddedDocuments("Tile", updates);
}

async function handleEvents(html) {
    html.find("a.direction-button").click(function(){
        const fine = html.find("input[name=control]")[0].checked;
        if(this.dataset.direction.includes("rotate")) rotate(this.dataset.direction, fine);
        else if(this.dataset.direction === "up" || this.dataset.direction === "down" ) moveY(this.dataset.direction, fine);
        else moveX(this.dataset.direction, fine);
    });
}

const style = `<style>
    #tile-mover-dialog .direction-button-box{
        display: flex;
        justify-content: space-evenly;
    }
    #tile-mover-dialog .direction-button {
            font-size: 2em;
            text-align: center;
            width: 60px;
            height: 40px;
            background-color: #bdbdbd8f;
            border-radius: 3px;
            line-height: 40px;
            border: 2px groove var(--color-border-light-highlight);
    }
</style>`;

const content = style + `<fieldset><legend>Move</legend><div class="direction-button-box">
    <a class="direction-button" data-direction="left"><i class="fas fa-long-arrow-alt-left"></i></a>
    <a class="direction-button" data-direction="up"><i class="fas fa-long-arrow-alt-up"></i></a>
    <a class="direction-button" data-direction="down"><i class="fas fa-long-arrow-alt-down"></i></a>
    <a class="direction-button" data-direction="right"><i class="fas fa-long-arrow-alt-right"></i></a>
</div></fieldset>
<hr>
<fieldset><legend>Rotate</legend><div class="direction-button-box">
    <a class="direction-button" data-direction="rotate-left"><i class="fas fa-undo"></i></a>
    <a class="direction-button" data-direction="rotate-reset"><i class="fab fa-creative-commons-zero"></i></a>
    <a class="direction-button" data-direction="rotate-right"><i class="fas fa-redo"></i></a>
</div></fieldset>
<hr>
<form>
    <div class="form-group">
        <label>Fine control?</label>
        <div class="form-fields">
            <input name="control" type="checkbox" checked/>
        </div>
    </div>
    <p>Fine <i class="fas fa-arrows-alt"></i>: 1 pixel, <i class="fas fa-redo"></i>: 5 degrees</p>
    <p>Normal <i class="fas fa-arrows-alt"></i>: 10 pixels, <i class="fas fa-redo"></i>: 45 degrees</p>
</form>
<hr>`;

let d = new Dialog({
    title: "Tile mover",
    content,
    buttons: {
        close: {
            label: "Close"
        }
    },
    render: handleEvents
},
{
    width: 280,
    id: "tile-mover-dialog"
});

d.render(true);
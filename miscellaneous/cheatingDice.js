const content = `
<form>
    <div class="form-group">
        <label>Dice Roll:</label>
        <div class="form-fields">
            <input name="formula" type="text" value="1d20">
        </div>
    </div>
    <div class="form-group">
        <label>Desired Result:</label>
        <div class="form-fields">
            <input name="outcome" type="number" value="20">
        </div>
    </div>
</form>
`;


new Dialog({
    title: "loaded dice dialog",
    content,
    buttons:{
        roll: {
            label: "Roll!",
            callback: cheatingDiceRoll
        }
    }
}).render(true);


// lets see if we can make this for any dice amount...
async function cheatingDiceRoll(html) {
    const formula = html.find("[name=formula]").val();
    const desired = Number(html.find("[name=outcome]").val());
    let roll = await new Roll(formula).evaluate();
    let diceDifference = desired - roll.total;
    let modifierTotal = roll.total - roll.dice.reduce((acc, e)=> acc += e.total, 0);
    const maxEyes = roll.dice.reduce((acc, e)=> acc += e.faces * e.number, 0);
    const minEyes = roll.dice.reduce((acc, e)=> acc += e.number, 0);
    if(desired < minEyes + modifierTotal || desired > maxEyes + modifierTotal) return ui.notifications.warn("Your cheated number is outside the range of possibility");
    while (diceDifference !== 0) {
        const dice = roll.terms.filter(e => e instanceof CONFIG.Dice.termTypes.DiceTerm);
        const change = diceDifference > 0 ? -1 : 1;
        for(let die of dice) {
            for(let result of die.results){
                if((result.result === 1 && change === 1) || (result.result === die.faces && change === -1) || diceDifference === 0) continue;
                result.result -= change;
                diceDifference += change;
            }
        }
    }
    roll._total = desired;
    await roll.toMessage();
}

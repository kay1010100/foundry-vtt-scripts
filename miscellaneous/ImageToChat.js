//v10+ macro (tested for v10 and v11)

function selectedPlayer(p) {
    if (p) {
        return {users: [p]};
    } else {
        return undefined;
    }
};

async function imageMessage(html, mode) {
    const {url, whisper, player} = new FormDataExtended(html[0].querySelector("form")).object;
    if (url === "") {
        return;
    }
    if (mode == "chat") {
        const messageContent = `<img src="${url}" />`;
        if(!whisper) return await ChatMessage.create({content: messageContent});

        const playerName = game.users.get(player).name;
        return await ChatMessage.create({content: messageContent, whisper: ChatMessage.getWhisperRecipients(playerName)});
    }
    if (mode == "popout"){
        const popout = new ImagePopout(url).render(true);
        return popout.shareImage(selectedPlayer(player));
    }

}

let dialogOptions = game.users.filter(u => u.role < 3).map(u => `<option value=${u.id}> ${u.name} </option>`).join(``);


let dialogContent = `<form>
                    <div><h2>Paste your image url below:</h2><div>
                    <div>URL: <input name="url" style="width:350px" autofocus></div>
                    <div><i>if the image is from the internet do not forget to include http(s):// in the url</i></div>
                    <div>Whisper to player?<input name="whisper" type="checkbox"/></div>
                    <div">Player name:<select name="player" disabled>${dialogOptions}</select></div>
                    </form>
                    `;
let stayOpen = false;
let d = new Dialog({
    title: "Image to Chat",
    content: dialogContent,
    buttons: {
        done: {
            label: "Send to Chat!",
            callback: async (html) => {
                stayOpen = true;
                await imageMessage(html, "chat");
            }
        },
        show: {
            label: "Show pop out!",
            callback: async (html) => {
                stayOpen = true;
                await imageMessage(html, "popout");
            }
        }
    },
    default: "done",
    close: () => {
        if(stayOpen) {
            stayOpen = false;
            d.render(true);
        }
        
    },
    render: (html) => {
        html.find("[name=whisper]").change(function () {
            if(html.find("[name=player]").attr("disabled")){
                html.find("[name=player]").removeAttr("disabled");
            }
            else {
               html.find("[name=player]").attr("disabled", true) ; 
            }
        });
    }
}).render(true);

// item macro that fires before the item is rolled.
// item is defined by the ItemMacro module, token is defined as the controlled token.
let ammoType = item.name.toLowerCase().includes("crossbow") ? "bolt" : "arrow";
const ammoChoices = token.actor.items
                    .filter(i => i.name.toLowerCase().includes(ammoType) && i.type === "consumable" && i.system.quantity > 0)
                    .sort((a,b)=> a.system.price.value - b.system.price.value)
                    .reduce((acc, i) => acc += `<option value="${i.id}">${i.name} | ${i.system.quantity} in the quiver`,``);

const content = `<form>
                    <div class="form-group">
                        <label>Ammo: </label>
                        <select class="ammo-selector">${ammoChoices}</select>
                    </div>
                </form>`;
new Dialog({
    title: "Ammo selector",
    content,
    buttons: {
        fire: {
            label: "Fire!",
            callback: async (html) => {
                const ammoId = html.find(".ammo-selector").val();
                await item.update({"system.consume.target": ammoId});
                item.roll();
            }
        },
        cancel: {
            label: "Cancel"
        }
    }
}).render(true);

// 5e party long rest command for the GM //
let macroActors = game.actors.filter(a => a.type == "character");
for (let macroActor of macroActors) {
    await macroActor.longRest({dialog: false, chat: true, newDay: true});
}

/**
 * defining a new Roll
 */
const roll = new Roll("1d6 + 5");

/**
 * evaluating (or rolling) a new Roll
 */
const asyncRoll = await new Roll("1d6 + 5").evaluate({async: true});
const syncRoll = new Roll("1d6 + 5").evaluate({async: false});
// .evaluate can be replaced with .roll both are identical to eachother.

/**
 * sending a roll directly to chat.
 */
const message = await new Roll("1d6 + 5").toMessage();
const privateMessage = await new Roll("1d6 + 5").toMessage({},{rollMode: CONST.DICE_ROLL_MODES.PRIVATE}); // aka a GM roll.
const blindMessage = await new Roll("1d6 + 5").toMessage({},{rollMode: CONST.DICE_ROLL_MODES.BLIND});     // aka a blind roll only visible for GM
const selfMessage = await new Roll("1d6 + 5").toMessage({},{rollMode: CONST.DICE_ROLL_MODES.SELF});       // aka a message ONLY to self (and GM depending on modules/settings)

/**
 * other toMessage options 
 */
const flavoredMessage = await new Roll("1d6 + 5").toMessage({flavor: "Bob's strike:"},{});                 // can contain HTML to make it look all nice and stuff!
const soundMessage = await new Roll("1d6 + 5").toMessage({sound: "assets/sounds/sfx/explosion.ogg"}, {});  // adds a different sound than the normal dice rolling sound.
const speakerMessage = await new Roll("1d6 + 5").toMessage({speaker: ChatMessage.getSpeaker({token: token.document})},{}); // changes the speaker from user to the token.

/**
 * combining the above neatly.
 */
const message = await new Roll("1d6 + 5").toMessage({
    flavor: "Bob's strike:",
    sound: "assets/sounds/sfx/explosion.ogg",
    speaker: ChatMessage.getSpeaker({token: token.document})
},{
    rollMode: CONST.DICE_ROLL_MODES.PRIVATE
});
// a few examples of things we can do with playlists and the sounds they contain.

/*
 * starting a playlist by name or id by playing all sounds, 
 * note if the playlist is set to simulataneous playback
 * all sounds play at once...
 */ 
const pl = game.playlists.getName("My Epic Music");  // .get("id of playlist works too")
await pl.playAll();

/*
 * Now if a playlist is playing we might want to stop that one first...
 */
const pl = game.playlists.find(p => p.playing);
if(pl) {
    await pl.stopAll();
}
// start a different playlist playing?

/*
 * But what if we want a specific sound playing? Well this following
 * works best on a soundboard playback mode playlist. Otherwise FoundryVTT
 * continues on to the next sound when it is done playing this specific one.
 */
const pl = game.playlists.getName("My Epic Music");
const sound = pl.sounds.getName("Epic Sound 1"); // get("id of sound") works too.
await pl.playSound(sound);

/*
 * Stopping a specific sound works quite similar again to stopping a complete playlist.
 */
const pl = game.playlists.getName("My Epic Music");
const sound = pl.sounds.find(s => s.name === "Epic Sound 1" && s.playing); // just making sure it is playing
if(sound){
    await pl.stopSound(sound);
}

/*
 * creating a sound in an existing playlist
 */
const pl = game.playlists.getName("My Epic Music");
const soundData = {
    name: "Epic Sound 15", 
    path: "assets/sound/music/Epic_sound_15.ogg", 
    volume: 0.1, 
    repeat: false
}
await pl.createEmbeddedDocuments("PlaylistSound", [soundData]);

/*
 * updating some sounds in an existing playlist
 */
const pl = game.playlists.getName("My Epic Music");
const updates = pl.sounds.filter(s => s.volume > 0.1).map(s => ({_id: s.id, volume: 0.1})); // creates an array of update objects from all sounds that have a volume over 0.1
await pl.updateEmbeddedDocuments("PlaylistSound", updates);

/*
 * deleting sounds out of an existing playlist
 */
const pl = game.playlists.getName("My Epic Music");
const deleteIds = pl.sounds.filter(s => !s.name.toLowerCase().includes("epic")).map(s => s.id); // creates an array of sound ids of sounds that dont have the word epic in them.
await pl.deleteEmbeddedDocuments("PlaylistSound", deleteIds);

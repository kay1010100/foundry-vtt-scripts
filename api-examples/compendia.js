/**
 * how to get your pack key!
 */
console.log(game.packs.keys()); // then open the console (F12) and find your compendium key in the list should look like "world.my-compendium-name" or "system.system-compendium-name" etc.

/**
 * getting a document from a compendium
 */
const key = "world.my-compendium-of-items";
const pack = game.packs.get(key);
const itemId = pack.index.getName("Bob's magic gauntlet")._id;
const myItem = await pack.getDocument(itemId);
// do something with myItem

/**
 * getting all documents from a compendium
 */
const key = "world.my-compendium-of-items";
const pack = game.packs.get(key);
const items = pack.getDocuments();
// do something with the array of items.

/**
 * create a different index with more keys than id and name
 */
const key = "world.my-compendium-of-items";
const pack = game.packs.get(key);
const index = await pack.getIndex({fields: ["type"]}); // creates an index of the compendium that includes the type field.
const itemIds = index.filter(i => i.type ==="spell").map(i => i._id); // creates an array of ids from the compendium of items that are spells (can differ from system to system)
let items = [];
for(let id of itemIds) {
    items.push(await pack.getDocument(id));
}
// do something with this items array.

/**
 * unlocking a compendium
 */
const key = "world.my-compendium-of-items";
const pack = game.packs.get(key);
await pack.configure({locked: false}); // true for locking it.

/**
 * making a compendium visible to all
 */
const key = "world.my-compendium-of-items";
const pack = game.packs.get(key);
await pack.configure({private: false}); // true for hiding it from players.

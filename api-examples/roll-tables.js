/**
 * All examples below assume a table with names as text entries, from a basic (core) roll table
 * Draw from a table
 */
const table = game.tables.getName("Orc Names");
const roll = await table.draw();

/**
 * Roll a table, no result shown in chat.
 */
const table = game.tables.getName("Orc Names");
const roll = await table.roll();

/**
 * Roll a table and create a custom chat message with the result
 */
const table = game.tables.getName("Orc Names");
const roll = await table.roll();
const message = roll.results.reduce((acc, e)=> acc += `<p>This Orc is called ${e.getChatText()}</p>`,``);
await ChatMessage.create({
    content: message,
    type: CONST.CHAT_MESSAGE_TYPES.IC,
});

/**
  * Draw many results at once from a table
  */
const num = 5;
const table = game.tables.getName("Orc Names");
const roll = await table.drawMany(num);

/**
 * Draw many results at once from a table and make a custom chat message that is whispered to the GM.
 */
const num = 5;
const table = game.tables.getName("Orc Names");
const roll = await table.drawMany(num, {displayChat: false});
const message = roll.results.reduce((acc, e, i) => acc += `<p>Orc ${i+1}: ${e.getChatText()}</p>`,``);
await ChatMessage.create({
    content: message,
    whisper: ChatMessage.getWhisperRecipients("GM"),
});

/**
 * Case where a GM rolls a table in secret with one player (Janet) recieving the result too.
 * Code by: Symon S.#4655 on discord
 */
const table = game.tables.getName("Orc Names");
const {roll, results} = await table.roll()
const messageData = new ChatMessage({whisper:ChatMessage.getWhisperRecipients('Janet')})
await table.toMessage(results,{roll: new Roll(roll.result),messageData});
